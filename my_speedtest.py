import speedtest


def test():
    result = []
    s = speedtest.Speedtest()
    s.get_servers()
    s.get_best_server()
    s.download()
    s.upload()
    res = s.results.dict()
    i = res["client"]["ip"]
    s = res["client"]["isp"]
    l = res["server"]["name"]
    t = res["server"]["sponsor"]
    d = res["download"]
    u = res["upload"]
    p = res["ping"]
    # print(res)
    result.append(f"Your IP Address {i}")
    result.append(f"Service Provider {s}")
    result.append(f"Test done on {l} location, to {t} Providers Server")
    result.append("Download speed {:.2f} Kb/s".format(d / 1024))
    result.append("Upload speed {:.2f} Kb/s".format(u / 1024))
    result.append(f"Ping Letency {p}")
    return result


if __name__ == "__main__":
    test()
