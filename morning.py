#!/usr/bin/ python
import pyttsx3
import speech_recognition as sr
import datetime
import os

# import speedtest
import my_speedtest as test

# import networktest
import my_networktest as networktest

engine = pyttsx3.init("sapi5")
# print(voices)
# voices = engine.getProperty("voices")

engine.setProperty(
    "voice",
    "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Speech\Voices\Tokens\TTS_MS_EN-US_ZIRA_11.0",
)


def speak(audio):
    # pass
    engine.say(audio)
    engine.runAndWait()


def wishMe():
    hour = int(datetime.datetime.now().hour)
    if hour >= 0 and hour < 12:
        speak("Good Morning")
    elif hour >= 12 and hour < 18:
        speak("Good Afternoon")
    else:
        speak("Good Evening")


def seeOnline():
    i = "10.10.0.1"
    responce = os.popen("ping " + i).read()
    if "TTL" in responce:
        speak("Online and ready Sir")
        return True
    else:
        speak("Not Able to communicate over network, please check network connectivity")
        input("Press Enter to continue...")
        return False


def getDateTime():
    currentDT = datetime.datetime.now()
    day = currentDT.strftime("%a, %b %d, %Y")
    time = currentDT.strftime("%I:%M:%S %p")
    speak(f"Today is {day} and current time is {time}")


def networkTesting():
    speak("Running network test, it might take some time, please wait...")
    result = networktest.networktest()
    for i in result:
        print(i)
        speak(i)


def speedTesting():
    # Speed Test
    speak("Perfoming speedtest, please wait...")
    result = test.test()
    for i in result:
        print(i)
        speak(i)


if __name__ == "__main__":
    wishMe()
    while True:
        online = seeOnline()
        if online == True:
            break
    getDateTime()
    networkTesting()
    speedTesting()
    speak("Have Good Day Sir")

