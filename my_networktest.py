#!/usr/bin/ python
import os
import csv


def networktest():
    result = []
    vset = 0
    uset = 0
    dset = 0
    try:
        f = open("testaddress.csv")
    except Exception as e:
        result.append(f"Sorry testaddress.csv File not found")
        result.append(
            f"Please Copy and rename testaddress_template.csv file to testaddress.csv and add your host and ip in list"
        )
    else:
        with f:
            csv_reader = csv.reader(f, delimiter=",")
            line_count = 0
            for row in csv_reader:
                if line_count == 0:
                    line_count += 1
                else:
                    vset += 1
                    responce = os.popen("ping " + row[1]).read()
                    if "TTL" in responce:
                        # print(f"{n} is up")
                        uset += 1
                        result.append(f"{row[0]} is up")
                    else:
                        # print(f"{n} is down")
                        dset += 1
                        result.append(f"{row[0]} is down")
                    line_count += 1
            result.append(f"Network Test is completed on {vset} Node")
            result.append(
                f"Total Down count is {dset} Node, and Up and runing {uset} node "
            )

    return result


if __name__ == "__main__":
    networktest()

